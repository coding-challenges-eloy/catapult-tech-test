import { combineReducers } from "redux";
import createStore from "./createStore";
import shiftsReducer from "./shiftsReducer";
import invitedContractsReducer from "./invitedContractsReducer";

const catapultReducer = combineReducers({
  invitedContracts: invitedContractsReducer,
  shifts: shiftsReducer
});

export default createStore(catapultReducer);
