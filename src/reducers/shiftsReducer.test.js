import shiftsReducer from "./shiftsReducer";

test("set a new filter", () => {
  const newState = shiftsReducer(
    {},
    { type: "SET_SHIFT_FILTER", payload: { filterType: "filterType" } }
  );

  expect(newState.shiftsListFilters).toHaveProperty("filterType");
});

test("set a new value for an existing filter", () => {
  const newState = shiftsReducer(
    {
      shiftsListFilters: {
        filterType: { filterType: "filterType", value: "initial" }
      }
    },
    {
      type: "SET_SHIFT_FILTER",
      payload: { filterType: "filterType", value: "final" }
    }
  );

  expect(newState).toHaveProperty(
    "shiftsListFilters.filterType.value",
    "final"
  );
});
