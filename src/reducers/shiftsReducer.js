const InitialState = {
  shiftsList: [],
  shiftsListFilters: {},
  fetchingShifts: false
};

const shiftsReducer = (state = InitialState, action) => {
  switch (action.type) {
    case "FETCH_SHIFTS_LIST_FULFILLED": {
      const {
        payload: { data }
      } = action;
      return Object.assign({}, state, {
        fetchingShifts: false,
        shiftsList: data
      });
    }

    case "FETCH_SHIFTS_LIST_PENDING": {
      return Object.assign({}, state, { fetchingShifts: true });
    }

    case "SET_SHIFT_FILTER": {
      return Object.assign({}, state, {
        shiftsListFilters: {
          ...state.shiftsListFilters,
          [action.payload.filterType]: action.payload
        }
      });
    }

    default:
      return state;
  }
};

export default shiftsReducer;
