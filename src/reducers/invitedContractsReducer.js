const InitialState = {
  invitedContractsList: [],
  roleFetched: 0
};

const invitedContractsReducer = (state = InitialState, action) => {
  switch (action.type) {
    case "FETCH_INVITED_CONTRACTS_LIST_FULFILLED": {
      const {
        payload: {
          data,
          config: {
            params: { roleId }
          }
        }
      } = action;
      return Object.assign({}, state, {
        fetchingInvitedContracts: false,
        invitedContractsList: data,
        roleFetched: roleId
      });
    }

    default:
      return state;
  }
};

export default invitedContractsReducer;
