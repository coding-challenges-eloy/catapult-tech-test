import { connect } from "react-redux";
import React, { Component, Fragment } from "react";
import { fetchInvitedContracts } from "../../actions/invitedContracts";
import PropTypes from "prop-types";
import InvitedContractsList from "./InvitedContractsList";

class InvitedContractsContainer extends Component {
  handleContractsClick(roleId) {
    console.log(roleId);
    this.props.fetchInvitedContractsList(roleId);
  }

  render() {
    const { invitedContractsList, roleFetched, roleId } = this.props;

    return (
      <Fragment>
        {roleFetched !== roleId && (
          <button onClick={() => this.handleContractsClick(roleId)}>
            Invited Candidates
          </button>
        )}
        {roleFetched === roleId && (
          <InvitedContractsList list={invitedContractsList} />
        )}
      </Fragment>
    );
  }
}

InvitedContractsContainer.propTypes = {
  invitedContractsList: PropTypes.arrayOf(
    PropTypes.shape({
      candidateName: PropTypes.string.isRequired
    })
  ),
  roleId: PropTypes.number.isRequired
};

const mapStateToProps = state => ({
  invitedContractsList: state.invitedContracts.invitedContractsList,
  roleFetched: state.invitedContracts.roleFetched
});

const mapDispatchToProps = dispatch => ({
  fetchInvitedContractsList: roleId => dispatch(fetchInvitedContracts(roleId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InvitedContractsContainer);
