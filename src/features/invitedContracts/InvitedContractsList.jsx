import PropTypes from "prop-types";
import React from "react";

function InvitedContractsList({ list }) {
  return (
    <ul>
      <h3>Invited Contracts</h3>
      {list.map(item => (
        <li key={item.id}>{item.candidateName}</li>
      ))}
      {!list.length && <div>There is no candidates for this shift.</div>}
    </ul>
  );
}

InvitedContractsList.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      candidateName: PropTypes.string.isRequired
    })
  ).isRequired
};

export default InvitedContractsList;
