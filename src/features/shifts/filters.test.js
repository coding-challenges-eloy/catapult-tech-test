import { applyFilters } from "./filters";

const LIST_MOCK = [
  { name: "Hello World", time: "09:00" },
  { name: "New World", time: "13:00" }
];

test("simple filter by full text search", () => {
  const filteredList = applyFilters(LIST_MOCK, {
    fullText: { name: "name", value: "lo" }
  });

  expect(filteredList.length).toBe(1);
});

test("simple filter by time range", () => {
  const filteredList = applyFilters(LIST_MOCK, {
    timeRangeAmPm: { name: "time", value: "AM" }
  });

  expect(filteredList.length).toBe(1);
});

test("filters combination: full text and time range", () => {
  const filteredList = applyFilters(LIST_MOCK, {
    fullText: { name: "name", value: "h" },
    timeRangeAmPm: { name: "time", value: "PM" }
  });

  expect(filteredList.length).toBe(0);
});

test("resets time range filter", () => {
  const filteredList = applyFilters(LIST_MOCK, {
    timeRangeAmPm: { name: "time", value: "Any" }
  });

  expect(filteredList.length).toBe(2);
});
