import PropTypes from "prop-types";
import React from "react";

/**
 * @func onChangeHandler
 *
 * @param {Function} cb
 * @return {Function} DOM event handler
 */
const onChangeHandler = cb => ({
  target: {
    name,
    type,
    value,
    dataset: { filter }
  }
}) => cb({ name, type, value, filterType: filter });
/**
 * @callback onChangeHandler~Callback
 * @param {Object} filter
 * @param {String} filter.name
 * @param {String} filter.value
 */

function ShiftsListFilters(props) {
  const { onChange } = props;

  return (
    <div>
      <h2>Filter by</h2>
      <label htmlFor="jobType">Job Type: </label>
      <input
        type="text"
        name="jobType"
        data-filter="fullText"
        onChange={onChangeHandler(onChange)}
      />
      <br />
      <label htmlFor="startTime">Start time: </label>
      <input
        type="radio"
        name="startTime"
        data-filter="timeRangeAmPm"
        onChange={onChangeHandler(onChange)}
        value="AM"
      />
      AM{" - "}
      <input
        type="radio"
        name="startTime"
        data-filter="timeRangeAmPm"
        onChange={onChangeHandler(onChange)}
        value="PM"
      />
      PM{" - "}
      <input
        type="radio"
        name="startTime"
        data-filter="timeRangeAmPm"
        onChange={onChangeHandler(onChange)}
        value="ALL"
      />
      All <hr />
    </div>
  );
}

ShiftsListFilters.propTypes = {
  onChange: PropTypes.func.isRequired
};

export default ShiftsListFilters;
