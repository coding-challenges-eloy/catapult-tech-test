const fullText = (list, name, value) =>
  list.filter(
    item => item[name].toLowerCase().indexOf(value.toLowerCase()) !== -1
  );

const timeRangeAmPm = (list, name, value) =>
  list.filter(item => {
    const hour = Number(item[name].split(":").shift());
    if (value === "AM") {
      return hour < 12;
    } else if (value === "PM") {
      return hour >= 12;
    } else {
      return true;
    }
  });

const FILTERS = { fullText, timeRangeAmPm };

export function applyFilters(list, filters) {
  let tmpList = list;
  for (const filterType in filters) {
    const { name, value } = filters[filterType];
    tmpList = FILTERS[filterType](tmpList, name, value);
  }
  return tmpList;
}
