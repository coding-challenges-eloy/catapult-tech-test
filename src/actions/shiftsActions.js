import { FETCH_SHIFTS_LIST, SET_SHIFT_FILTER } from "./constants.js";
import { fetchShiftsList } from "../lib/requests/webapi.js";

export function fetchShifts() {
  return {
    type: FETCH_SHIFTS_LIST,
    payload: fetchShiftsList()
  };
}

export function setShiftsFilter(payload) {
  return {
    type: SET_SHIFT_FILTER,
    payload
  };
}
